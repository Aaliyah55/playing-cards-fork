# include <iostream>
# include <conio.h>


using namespace std;
enum Rank
{
	Two = 2,
	Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit 
{
	Spades, Hearts, Clubs, Diamonds,
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card c)
{

	switch (c.rank)
	{
	case 2:
		cout << "Two of \n";
		break;
	case 3:
		cout << "Three of \n";
		break;
	case 4:
		cout << "Four of \n";
		break;
	case 5:
		cout << "Five of \n";
		break;
	case 6:
		cout << "Six of \n";
		break;
	case 7:
		cout << "Seven of \n";
		break;
	case 8:
		cout << "Eight of \n";
		break;
	case 9:
		cout << "Nine of \n";
		break;
	case 10:
		cout << "Ten of \n";
		break;
	case 11:
		cout << "Jack of \n";
		break;
	case 12:
		cout << "Queen of \n";
		break;
	case 13:
		cout << "King of \n";
		break;
	case 14:
		cout << "Ace of \n";
		break;
	}

	switch (c.suit)
	{
	case Hearts:
		cout << "Hearts";
		break;
	case Spades:
		cout << "Spades";
		break;
	case Diamonds:
		cout << "Diamonds";
		break;
	case Clubs:
		cout << "Clubs";
		break;
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	else if (card2.rank > card1.rank)
	{
		return card2;
	}
	else
	{
		if (card1.suit > card2.suit)
		{
			return card1;
		}
		else if (card2.suit > card1.suit)
		{
			return card2;
		}
	}
}

int main()
{
	Card a;
	a.rank = Jack;
	a.suit= Hearts;

	Card b;
	b.rank = Three;
	b.suit = Diamonds;

	Card c;
	c.rank = Ten;
	c.suit = Spades;

	Card d;
	d.rank = Ace;
	d.suit = Diamonds;



	PrintCard(HighCard(a, b));




	(void)_getch();
	return 0;
}